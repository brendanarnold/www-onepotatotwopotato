
$(document).ready(function() {

  // Could pre-record image widths or use a more complex image loading
  // library e.g. https://github.com/desandro/imagesloaded/blob/master/imagesloaded.js
  // but is simpler and quicker to include some approximate widths
  var AVG_IMG_WIDTH = 450;
  var AVG_IMG_HEIGHT = 550;

  $(".img-container").each(function(i) {
    // Scope var declarations
    var rndLeft, rndTop, imgDiv;
    imgDiv = $(this);
    // Position each image randomly on page and then reveal
    rndLeft = getRndXPos(imgDiv.width());
    rndTop = getRndYPos(imgDiv.height());
    imgDiv.css( { 
      "top" : rndTop + "px", 
      "left" : rndLeft + "px",
      "visibility" : "visible",
    } );
  });
  // Some nice easy extra functionality from jQueryUI
  $(".img-container").draggable();

  function getRndXPos() {
    var res = Math.random() * ($(window).width() - AVG_IMG_WIDTH);
    return Math.floor(res);
  }

  function getRndYPos() {
    var res = Math.random() * ($(window).height() - AVG_IMG_HEIGHT);
    return Math.floor(res);
  }

});
