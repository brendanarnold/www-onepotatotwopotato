from django.conf.urls import patterns, url

from spuds import views

urlpatterns = patterns('',
    url(r'^$', views.index, name="index"),
    url(r'^show/(?P<num>\d+)/$', views.show, name="show"),
)
