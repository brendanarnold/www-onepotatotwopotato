# Create your views here.

from django.shortcuts import render

def show(request, num):
    num = int(num)
    if num > 4:
        num = 4
    if num < 0:
        num = 0
    # FIXME: Really this data should be defined in the view, perhaps
    # using a custom template tag - see
    # https://docs.djangoproject.com/en/1.5/howto/custom-template-tags/#writing-custom-template-tags 
    img_fns = ['wafflecopter.png', 'potatosalad.png', 'hypnochips.png', 'spudnik.png']
    img_fns = img_fns[:num]
    return render(request, 'spuds/show.html', { 'img_fns': img_fns })

def index(request):
    return render(request, 'spuds/index.html')
