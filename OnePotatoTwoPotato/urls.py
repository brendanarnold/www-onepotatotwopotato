from django.conf.urls import patterns, include, url
import OnePotatoTwoPotato.settings as settings

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # FIXME This actually maps two separate URLs to the same things - may be
    # problem with SEO here ...
    url(r'^/?', include('spuds.urls', namespace="spuds")),
    url(r'^spuds/', include('spuds.urls', namespace="spuds")),
    # url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
